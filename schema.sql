CREATE TABLE IF NOT EXISTS person (
  inn_number text PRIMARY KEY,
  first_name text NOT NULL,
  middle_name text NOT NULL,
  last_name text NOT NULL,
  dob TEXT NOT NULL,
  pob TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS document (
  document_type TEXT NOT NULL,
  owner TEXT NOT NULL,
  series text NOT NULL,
  number text NOT NULL,
  date_of_issue TEXT NOT NULL,
  issued_by TEXT NOT NULL,
  PRIMARY KEY (document_type, owner),
  FOREIGN KEY (owner) REFERENCES person(inn_number)
);

drop table if exists inspection;
CREATE TABLE IF NOT EXISTS inspection (
  code text NOT NULL PRIMARY KEY,
  name text NOT NULL
);

drop table if exists income;
CREATE TABLE IF NOT EXISTS income (
  code text NOT NULL PRIMARY KEY,
  name text NOT NULL
);

