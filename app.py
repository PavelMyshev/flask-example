import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
    render_template, flash
from contextlib import closing


# configuration
DATABASE = 'decl.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'admin'

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route('/')
def show_entries():
    cur = g.db.execute('select * from person')
    entries = [dict(inn_number=row[0], first_name=row[1], middle_name=row[2],
                    last_name=row[3], dob=row[4], pob=row[5]) for row in cur.fetchall()]
    return render_template('show_entries.html', entries=entries)


@app.route('/add', methods=['POST'])
def add_entry():
    g.db.execute('insert into person (last_name, first_name, middle_name, inn_number, dob, pob) values (?, ?, ?, ?, ?, ?)',
                 [request.form['last_name'], request.form['first_name'], request.form['middle_name'],
                  request.form['inn_number'], request.form['dob'], request.form['pob']])
    g.db.commit()
    flash('Данные успешно добавлены')
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Неверное имя пользователя'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Неверный пароль'
        else:
            session['logged_in'] = True
            flash('Вход выполнен успешно')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('Вы вышли')
    return redirect(url_for('show_entries'))

@app.route('/info')
def info():
    return redirect(url_for('show_entries'))

@app.route('/add_inspection', methods=['POST'])
def add_inspection():
    g.db.execute('insert into inspection (code, name) values (?, ?)',
                 [request.form['code'], request.form['name']])
    g.db.commit()
    flash('Данные успешно добавлены')
    return redirect(url_for('show_inspection'))


@app.route('/show_inspection')
def show_inspection():
    cur = g.db.execute('select * from inspection')
    inspections = [dict(code=row[0], name=row[1]) for row in cur.fetchall()]
    return render_template('show_inspection.html', inspections=inspections)

@app.route('/add_income', methods=['POST'])
def add_income():
    g.db.execute('insert into income (code, name) values (?, ?)',
                 [request.form['code'], request.form['name']])
    g.db.commit()
    flash('Данные успешно добавлены')
    return redirect(url_for('show_income'))


@app.route('/show_income')
def show_income():
    cur = g.db.execute('select * from income')
    incomes = [dict(code=row[0], name=row[1]) for row in cur.fetchall()]
    return render_template('show_income.html', incomes=incomes)

@app.route('/add_documents', methods=['POST'])
def add_documents():
    g.db.execute('insert into document (document_type, owner, series, number, date_of_issue, issued_by) values (?, ?, ?, ?, ?, ?)',
                 [request.form['document_type'], request.form['owner'], request.form['series'],
                  request.form['number'], request.form['date_of_issue'], request.form['issued_by']])
    g.db.commit()
    flash('Данные успешно добавлены')
    return redirect(url_for('show_documents'))


@app.route('/show_documents')
def show_documents():
    cur = g.db.execute('select inn_number from person')
    inn_list = [dict(inn=row[0]) for row in cur.fetchall()]
    return render_template('show_documents.html', inn_list=inn_list)

@app.route('/delete_all_data')
def delete_all_data():
    g.db.executescript('delete from income;delete from inspection;delete from document;delete from person;')
    g.db.commit()
    flash('Все данные успешно удалены')
    return redirect(url_for('show_entries'))

@app.route('/select_docs', methods=['POST'])
def select_docs():
    cur = g.db.execute('select document_type, series, number from document where owner=?',
                 [request.form['select_owner']])
    docs_list = [dict(document_type=row[0], series=row[1], number=row[2]) for row in cur.fetchall()]
    cur1 = g.db.execute('select inn_number from person')
    inn_list = [dict(inn=row[0]) for row in cur1.fetchall()]
    owmer_name = ''.join([request.form['select_owner']])

    return render_template('show_documents.html', docs_list=docs_list,inn_list=inn_list, owmer_name=owmer_name)

init_db()
if __name__ == '__main__':

    app.run()